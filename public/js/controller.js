
var debug;

document.addEventListener("DOMContentLoaded", function() {

	//
	// Initialization
	//

	// Assign query selector to the dollar sign if nobody's using it
	var $ = $ || function(selector, node) {

		node = node || document;
		return node.querySelector(selector);
	};

	// Scoping some variables for use later
	var addForm, delForm, editForm, queue, queueItem, refresh,
		statusBox, statusClass, statusShowClass;

	// Get the "add video" form and queue for referencing later
	addForm = $("#add-url");
	queue = $("#queue").getElementsByTagName("tbody").item(0);

	// Create the status area & classes, and append it to the body
	statusBox = Make("div");
	statusClass = "status";
	statusShowClass = "status-visible";
	statusBox.className = statusClass;
	document.body.appendChild(statusBox);
	
	// Set up the automatic queue refresh
	//refresh = setTimeout(60000, UpdateQueue);


	//
	// Add behavior to form controls
	//

	// Handle "Add URL" form submissions
	addForm.addEventListener("submit", function(e) {
		AddVideo(
			this.elements["url"].value,
			this.elements["title"].value
		);

		e.preventDefault();
	});

	// Handle "edit downloader" form
	//$(".column-edit form").addEventListener("submit", function(e) {
		//AddURL($("").value);
	//}, true);


	// Preload the queue
	UpdateQueue();

	//
	// Primary Functions
	//

	// Add a video to the queue
	function AddVideo(url, title) {

		SendData("/create",
		{
			"title": title,
			"url": url
		},
		function() {
			DisplaySuccess("Video successfully queued.");
			UpdateQueue();
		},
		function() {
			DisplayError("Unable to add video to the queue!");
		});
	}


	// Present an error message to the user
	function DisplayError(msg) {
		DisplayStatus(msg, "status-bad");
	}


	// Present a status/success message to the user
	function DisplaySuccess(msg) {
		DisplayStatus(msg, "status-good");
	}


	// Remove a video from the queue
	function RemoveVideo(url) {

		SendData("/delete",
		{"url": url},
		function() {
			DisplaySuccess("Video successfully removed.");
			UpdateQueue();
		},
		function() {
			DisplayError("Unable to remove the video!");
		});
	}


	// Update the queue from the server
	function UpdateQueue() {
		GetData("/videos",
		function(data) {
			FillQueue(data);
		},
		function() {
			DisplayError("Unable to refresh queue from server");
		});
	}


	//
	// Support Functions
	//

	// Present a successful/status message to the user
	function DisplayStatus(msg, displayClass) {
		statusBox.innerHTML = msg;
		statusBox.className = statusClass+" "+statusShowClass+" "+displayClass;
		window.setTimeout(HideStatus, 3000);
	}


	// Populates the queue table with data
	function FillQueue(data) {

		// Clear the queue
		queue.innerHTML = "";

		// Add all the new items to the queue
		for(url in data) {
			queue.appendChild(MakeQueueItem(url, data[url]));
		}
	}


	// Quicky AJAX get
	function GetData(url, success, failure) {

		var get = new XMLHttpRequest();

		get.onload = function() {

			if(get.status == 200) {

				success(JSON.parse(get.responseText));
			}
			else if(failure) {

				failure();
			}
		};

		get.open("get", url, true);
		get.send();
	}


	// Hide the status box
	function HideStatus() {

		// Save the existing classes, to preserve appearance on animation
		var classes = statusBox.className;	
		classes = classes.split(" ");

		// Remove the "show me" class
		var rmClass = classes.indexOf(statusShowClass);

		if(rmClass >= 0) {
			delete classes[rmClass];
		}

		statusBox.className = classes.join(" ");
	}


	// Create a new node
	function Make(type) {

		return document.createElement(type);
	}


	// Create a DOM node for the "delete" button
	function MakeDelForm(url) {

		if(!delForm) {

			delForm = Make("form");
			delForm.action = "/delete";
			delForm.method = "post";

			var delUrl = Make("input");
			delUrl.name = "url";
			delUrl.type = "hidden";

			var submit = Make("input");
			submit.type = "submit";
			submit.value = "✗";

			delForm.appendChild(delUrl);
			delForm.appendChild(submit);
		}

		// Assign the information to the HTML form
		var delThis = delForm.cloneNode(true);
		var delThisUrl = $("input[type=hidden]", delThis);	

		delThisUrl.value = url;

		// Handle "Delete" requests
		delThis.addEventListener("submit", function(e) {

			RemoveVideo(this.elements["url"].value);

			e.preventDefault();

		}, true);

		return delThis;
	}


	// Create a DOM node for the "edit" button
	function MakeEditForm(url) {

		if(!editForm) {

			editForm = Make("form");
			editForm.action = "/update";
			editForm.method = "post";

			var editUrl = Make("input");
			editUrl.name = "url";
			editUrl.type = "hidden";

			var submit = Make("input");
			submit.type = "submit";
			submit.value = "✎";

			editForm.appendChild(editUrl);
			editForm.appendChild(submit);
		}

		// Assign the information to the HTML form
		var editThis = editForm.cloneNode(true);
		var editThisUrl = $("input[type=hidden]", editThis);	

		editThisUrl.value = url;

		// Handle "Delete" requests
		editThis.addEventListener("submit", function(e) {

			//EditVideo(this.elements["url"].value);

			e.preventDefault();

		}, true);


		return editThis;
	}



	// MakeQueueItem
	function MakeQueueItem(url, desc) {

/* Example queue item (v1)
				<tr class="queue-item">
					<td class="column-desc">Kids react to being eaten by a dinosaur</td>
					<td class="column-edit">
						<form action="/update" method="post">
							<input type="hidden" value="http://url" name="video-url">
							<input type="submit" value="✎" class="button button-edit">
						</form>
					</td>
					<td class="column-delete">
						<form action="/delete" method="post">
							<input type="hidden" value="http://url" name="video-url">
							<input type="submit" value="✗" class="button button-delete">
						</form>
					</td>
				</tr>
*/
		if(!queueItem) {

			// Fill the queue item row with a skeleton DOM structure
			queueItem = Make("tr");
			queueItem.className = "queue-item";

			var queueDesc = Make("td");
			var queueEdit = Make("td");
			var queueDel  = Make("td");

			queueDesc.className = "column-desc";
			queueEdit.className = "column-edit";
			queueDel.className = "column-delete";

			queueItem.appendChild(queueDesc);
			queueItem.appendChild(queueEdit);
			queueItem.appendChild(queueDel);
		}

		var thisItem = queueItem.cloneNode(true);
		var thisDesc = $(".column-desc", thisItem);
		var thisEdit = $(".column-edit", thisItem);
		var thisDel  = $(".column-delete", thisItem);

		// Populate the data for this downloader
		thisDesc.innerHTML = desc;
		thisDel.appendChild(MakeDelForm(url));
		thisEdit.appendChild(MakeEditForm(url));

		return thisItem;
	}


	// Easy peasy AJAX post
	function SendData(url, data, success, failure) {

		var ping = new XMLHttpRequest();
		var preparedData = new FormData();

		for(var i in data) {

			preparedData.append(i, data[i]);
		}

		ping.open("post", url);
		ping.onload = function(e) {

			if(ping.status === 200) {
				success();
			}
			else if(failure) {
				failure();
			}
		};

		ping.send(preparedData);
	}
});


